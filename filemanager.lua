-- file manager
--
-- basic file management for 
-- built-in storage and usb msd. 
-- copying and renaming files
--
-- poorly tested! dangerous!
--
-- first screen is a list of drives, 
-- "DUST" is 'home/we/dust' 
-- on internal storage
-- 
-- k2: unmount drive
--     use this before removing a
--     flash drive
-- k3: select drive
--
-- next screen has dirs/files on the
-- left, actions on the right
--
-- k2: go up
-- k3: execute action
-- e2: select file
-- e3: select action
--
-- to copy a file, you must first
-- mark the destination folder
-- then select the file and pick
-- 'copy' action.
-- to copy to the root of a drive
-- use the mark action on ".."

-- TODO:
-- - more bullet-proof code - probably ok for now
--   but using lua posix for stat etc would be nicer
-- - maintain "destination mark" between invocations
-- - sox operations on audio files 
--   (normalize, mix, stitch)
-- - graphical audio editing (not)
-- - vim clone for text editing (extremely not)
-- - make it nice enough to be part of 
--   the norns system menu instead of a script :)


UI = require("ui")
textentry = require('textentry')

local dialog = include('filemanager/lib/dialog')

local function shotgun(...)
  print(...)
end

local pickWidget = nil
local actionWidget = nil
local message = nil
local selection = nil
local menu_key1 = function() end
local menu_select = function() end
local menu_back = function() end

local function shell_cmd(name, cmd)
    message = name
    redraw()
    shotgun("executing", cmd)
    local res, code = os.execute(cmd)
    shotgun("result", res, code)
    message = nil
end

local function get_actions(dir, e)
  local function get_filetype(dir, e)
    if (string.match(e,"%.%.")) then
      return "up"
    elseif (string.match(e, "(empty)")) then
      return  "empty"
    elseif (string.match(e,"/$")) then
      return "dir"
    elseif (string.match(e, ".wav$") or string.match(e, ".WAV$")) then
      return "audio"
    else
      return "file"
    end
  end
  
  local action_names = {
    up = {"up", "mark", "make directory"},
    empty = {"up"},
    dir = {"open", "rename", "mark", "delete"},
    file = {"rename", "copy", "delete" },
    audio = {"rename", "copy", "delete" }
  }
  
  local t = get_filetype(dir, e)
  
  return { type = t, name = e, actions = action_names[t] }
end

local function go_up(dir, e)
  if (dir ~= "/home/we/dust/" and not string.match(dir, "/media/usb%d/$")) then
    local up = string.match(dir, "(/.+/)[^/]+")
    show_dir(up)
  else
    show_mounts()
  end
end

local function open_dir(dir, e)
  show_dir(dir .. e)
end

local function make_dir(dir)
  local function do_make_dir(s)
    if (s) then
      local cmd = string.format("mkdir \"%s\"; sync", dir .. s)
      shell_cmd("mkdir...", cmd)
      show_dir(dir)
      redraw()
    end
  end
  
  textentry.enter(do_make_dir, "new dir", "make directory in " .. dir)
end

local destination = nil;
local function mark(dir, e)
  if (e == "..") then
    destination = dir
  else
    destination = dir .. e;
  end
  shotgun("marked", destination)
end

local function delete(dir, e)
  local function do_delete(confirm)
    local bin = "rm"
    if (string.match(e, "/$")) then
      bin = "rmdir"
    end
    if (confirm) then
      local cmd = string.format("%s \"%s\"; sync", bin, dir .. e)
      shell_cmd("Deleting...", cmd)
      show_dir(dir)
      redraw()
    end
  end
  
  dialog.enter(do_delete, {"delete", dir .. e .. "?"}, "delete", "cancel")
end


local function rename(dir, e)
  local function do_rename(s)
    if (s) then
      local cmd = string.format("mv \"%s\" \"%s\"; sync", dir .. e, dir .. s)
      shell_cmd("Renaming...", cmd)
      show_dir(dir)
      redraw()
    end
  end
  
  local dirName = string.match(e, "(.*)/") or e
  
  textentry.enter(do_rename, dirName, "renaming " .. e)
end

local function copy(dir, e)
  if (destination) then
    local cmd = string.format("cp \"%s\" \"%s\"; sync", dir .. e, destination .. e)
    shell_cmd("Copying...", cmd)
    show_dir(dir)
    redraw()
  else
    dialog.enter(function () show_dir(dir) end, {"error: no marked destination", "to copy a file", "go to destination dir", "and select mark"}, "ok")
  end
end

local action_impl = {
  up = go_up, mark = mark, copy = copy, open = open_dir, rename = rename, mark = mark, delete = delete, ["make directory"] = make_dir
}


local old_dir = nil
local old_sel = nil

function show_dir(dir, sel)
  local files = { ".." }
  
  for i,v in pairs(util.scandir(dir)) do
    table.insert(files,v)
  end
  
  if (#files == 0) then
    table.insert(files, "(empty)")
  end
  
  pickWidget = UI.ScrollingList.new(0, 7, 1, files)
  
  pickWidget.on_update_selection = function(self)
    local e = files[self.index]

    selection = get_actions(dir, e)
    
    actionWidget = UI.ScrollingList.new(72, 7, 1, selection.actions)
    actionWidget.on_update_selection = function(index)
      selection.action = selection.actions[index]
    end
    
    actionWidget.on_update_selection(1)
  end

  pickWidget:set_index(sel or 1)
  pickWidget:on_update_selection()
  
  menu_key3 = function(i)
    local e = files[i]
    local a = selection.action
    
    local handler = action_impl[a]
    if (handler) then
      handler(dir, e)
    else
      shotgun("unimplemented", e, a)
      dialog.enter(function () show_dir(dir, i) end, {"unimplemented command", a}, "ok")
    end
    
  end
  
  menu_key2 = function(i)
    local e = files[i]
    go_up(dir, e)
  end
  
  menu_key1 = function() 
    old_dir = dir
    old_sel = pickWidget.index
    show_mounts()
  end
end

function show_mounts()
  local mountpoints = {"/home/we/dust/"}
  local labels = {"DUST"}
  
  local r = io.popen("mount -l | grep /media")
  
  for m in r:lines() do
    local mountpoint = string.match(m, "on (.*) type") .. "/"
    table.insert(mountpoints, mountpoint)
    local label = string.match(m, "%[(.*)%]")
    table.insert(labels, label)
  end
  
  pickWidget = UI.ScrollingList.new(0, 7, 1, labels)
  actionWidget = nil
  selection = nil
  
  menu_key3 = function(i)
    show_dir(mountpoints[i])
  end
  
  menu_key2 = function(i)
    local do_unmount = function(confirm)
      if (confirm) then
        local cmd = string.format("sudo umount " .. mountpoints[i])
        shotgun("executing", cmd)
        local res, code = os.execute(cmd)
        shotgun("result", res, code)
      end
      show_mounts()
    end
    
    if (i ~= 1) then
      dialog.enter(do_unmount, {"unmount", labels[i] .. " from " .. mountpoints[i], "?"}, "unmount", "cancel")
    end
  end
  
  menu_key1 = function() 
    if (old_dir) then
      show_dir(old_dir, old_sel)
    end
  end
end

function enc(n,d)
  if (n==2) then
    pickWidget:set_index_delta(d,true)
    if (pickWidget.on_update_selection) then
      pickWidget:on_update_selection()
    end
    redraw()
  end
  
  if (n==3) then
    actionWidget:set_index_delta(d,true)
    if (actionWidget.on_update_selection) then
      actionWidget.on_update_selection(actionWidget.index)
    end
    redraw()
  end
end

function key(n,z)
  if (n == 1 and z == 0) then
    menu_key1(pickWidget.index)
    redraw()
  end
  
  if (n == 2 and z == 0) then
    menu_key2(pickWidget.index)
    redraw()
  end
    
  if (n == 3 and z == 0) then
    menu_key3(pickWidget.index)
    redraw()
  end
end


function redraw()
  screen.clear()
  
  if (pickWidget) then
    pickWidget:redraw()
  end

  if (selection) then
    screen.rect(68,0,50,80)
    screen.level(0)
    screen.fill()
    screen.level(3)
    screen.move(70,0)
    screen.line(70,80)
    screen.stroke()
  end
  
  if (actionWidget) then
    actionWidget:redraw()
  end
  
  if (message) then
    screen.rect(10,10,108,44)
    screen.level(0)
    screen.fill()
    screen.rect(10,10,108,44)
    screen.level(15)
    screen.stroke()
    screen.move(64,32)
    screen.text_center(message)
  end
  
  screen.update()
end

show_mounts()
