-- dialog UI

local dialog = {}

dialog.enter = function(callback, text, rightopt, leftopt)
  dialog.text = text or ""
  dialog.leftopt = leftopt
  dialog.rightopt = rightopt or "ok"
  dialog.callback = callback
  
  dialog.key_restore = norns.menu.get_key()
  dialog.enc_restore = norns.menu.get_enc()
  dialog.redraw_restore = norns.menu.get_redraw()
  
  norns.menu.set(dialog.enc, dialog.key, dialog.redraw)
  redraw = dialog.redraw
  
  dialog.redraw()
end

dialog.exit = function(choice)
  norns.menu.set(dialog.enc_restore, dialog.key_restore, dialog.redraw_restore)
  redraw = dialog.redraw_restore
  _menu.set_mode(false)
  dialog.callback(choice)
  redraw()
end

dialog.key = function(n,z)
	if (n == 2 and z == 0) then
	  dialog.exit(false)
	elseif (n == 3 and z == 0) then
	  dialog.exit(true)
	end
end

dialog.enc = function(n,delta)
end

dialog.redraw = function()
  screen.clear()

  if (dialog.leftopt) then
    screen.move(0,54)
    screen.text(dialog.leftopt)
    screen.move(0,62)
    screen.text("[K2]")
  end
  screen.move(128,54)
  screen.text_right(dialog.rightopt)
  screen.move(128,62)
  screen.text_right("[K3]")
  
  if (type(dialog.text) == "string") then
    screen.move(64,24)
    screen.text_center(dialog.text)
  elseif (type(dialog.text) == "table") then
    for i, txt in pairs(dialog.text) do
      screen.move(64,10 + i*8)
      screen.text_center(txt)
    end
  end
  
  screen.update()
end

return dialog
